package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"
	"errors"

	flag "github.com/spf13/pflag"
)

type Inventory struct {
	Hosts map[string]time.Time `json:"hosts"`
}

func getIPFromString(s string) (net.IP, error) {
	remoteIP, _, err := net.SplitHostPort(s)
	if err != nil {
		msg := fmt.Sprintf("Could not parse IP %s", s)
		return nil, errors.New(msg)
	}
	ip := net.ParseIP(remoteIP)
	if ip == nil {
		msg := fmt.Sprintf("Could not parse IP %s", s)
		return nil, errors.New(msg)
	}
    return ip, nil
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	remoteIP, err := getIPFromString(r.RemoteAddr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		fmt.Printf("%s - %s request to %s from %s [400] - %s\n", time.Now().UTC(), r.Method, r.URL, r.RemoteAddr, err)
		return
	}
	if r.Method == "GET" {
		fmt.Printf("%s - %s request to %s from %s [200]\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
		return
	} else {
		http.Error(w, "Method is not supported.", http.StatusNotFound)
		fmt.Printf("%s - %s request to %s from %s [404] - Method is not supported\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
		return
	}
}

func inventoryHandler(inventory Inventory, proxyIPAllowList []net.IP, remoteHostHeaders []string, keepFor time.Duration) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
    	remoteIP, err := getIPFromString(r.RemoteAddr)
    	if err != nil {
    		http.Error(w, err.Error(), http.StatusBadRequest)
    		fmt.Printf("%s - %s request to %s from %s [400] - %s\n", time.Now().UTC(), r.Method, r.URL, r.RemoteAddr, err)
    		return
    	}
		if r.URL.Path != "/inventory" {
			http.Error(w, "404 not found.", http.StatusNotFound)
			fmt.Printf("%s - %s request to %s from %s [404]\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
			return
		}

		if r.Method == "GET" {
			now := time.Now().UTC()
			if keepFor > 0 {
				keepFor = keepFor * -1
			}
			past_time := now.Add(keepFor)
			for k, v := range inventory.Hosts {
				if v.Before(past_time) {
					delete(inventory.Hosts, k)
				}
			}
			json.NewEncoder(w).Encode(inventory)
			fmt.Printf("%s - %s request to %s from %s [200]\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
		} else if r.Method == "PUT" {
			for _, v := range proxyIPAllowList {
				if v.Equal(remoteIP) {
					for _, header := range remoteHostHeaders {
						if remoteAddress := r.Header.Get(header); remoteAddress != "" {
							remoteAddressIP := net.ParseIP(remoteAddress)
							if remoteAddressIP == nil {
		                        msg := fmt.Sprintf("Could not parse IP %s in header %s", remoteAddress, header)
    		                    http.Error(w, msg, http.StatusBadRequest)
                                fmt.Printf("%s - %s request to %s from %s [400] - %s\n", time.Now().UTC(), r.Method, r.URL, remoteIP, msg)
                                return
							} else if remoteAddressIP.IsLoopback() {
								msg := fmt.Sprintf("Refusing to update inventory with loopback address %s in header %s", remoteAddress, header)
    		                    http.Error(w, msg, http.StatusBadRequest)
                                fmt.Printf("%s - %s request to %s from %s [400] - %s\n", time.Now().UTC(), r.Method, r.URL, remoteIP, msg)
								return
							} else if remoteAddressIP.IsMulticast() {
								msg := fmt.Sprintf("Refusing to update inventory with multicast address %s in header %s", remoteAddress, header)
    		                    http.Error(w, msg, http.StatusBadRequest)
                                fmt.Printf("%s - %s request to %s from %s [400] - %s\n", time.Now().UTC(), r.Method, r.URL, remoteIP, msg)
								return
							} else {
    							utc := time.Now().UTC()
    							inventory.Hosts[remoteAddress] = utc
    							w.WriteHeader(http.StatusCreated)
    							fmt.Printf("%s - %s request to %s from %s [201] - Remote IP %s from %s\n", time.Now().UTC(), r.Method, r.URL, remoteIP, remoteAddress, header)
    							return
							}
						}
					}
					http.Error(w, "Request from Allowed Proxy without Remote Host Header", http.StatusBadRequest)
					fmt.Printf("%s - %s request to %s from %s [400] - Request from Allowed Proxy without Remote Host Header\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
					return
				}
			}
			// if we have a remote host header but not from a proxy deny the request
			for _, header := range remoteHostHeaders {
				if r.Header.Get(header) != "" {
					http.Error(w, fmt.Sprintf("Cannot specify header %s from %s", header, remoteIP), http.StatusBadRequest)
					fmt.Printf("%s - %s request to %s from %s [400] - Cannot specify header %s from %s\n", time.Now().UTC(), r.Method, r.URL, remoteIP, header, remoteIP)
					return
				}
			}
			if remoteIP.IsLoopback() {
				http.Error(w, "Refusing to update inventory with loopback", http.StatusBadRequest)
				fmt.Printf("%s - %s request to %s from %s [400] - Refusing to update inventory with loopback\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
				return
			} else if remoteIP.IsMulticast() {
				http.Error(w, "Refusing to update inventory with multicast address", http.StatusBadRequest)
				fmt.Printf("%s - %s request to %s from %s [400] - Refusing to update inventory with multicast address\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
				return
			}

			utc := time.Now().UTC()
			inventory.Hosts[remoteIP.String()] = utc
			w.WriteHeader(http.StatusCreated)
			fmt.Printf("%s - %s request to %s from %s [201]\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
		} else {
			http.Error(w, "Method is not supported.", http.StatusNotFound)
			fmt.Printf("%s - %s request to %s from %s [404] - Method is not supported\n", time.Now().UTC(), r.Method, r.URL, remoteIP)
			return
		}
	}
}

func main() {
	var port int
	var keepFor time.Duration
	var proxyIPAllowList []string
	var proxyIPAllowListIP []net.IP 
	var remoteHostHeaders []string

	flag.IntVarP(&port, "port", "p", 8080, "Specify a port. Default is 8080")
	flag.DurationVarP(&keepFor, "expires", "e", 2*time.Hour, "Specify the amount of time before an inventory item is expired")
	flag.StringArrayVarP(&proxyIPAllowList, "proxy-ip-allow", "P", []string{}, "Specify an IP to allow as a proxy host, multiple allowed")
	flag.StringArrayVarP(&remoteHostHeaders, "remote-host-header", "H", []string{}, "Specify a header that contains the remote host IP when request is proxied, multiple allowed")
	flag.Usage = func() {
		flag.PrintDefaults()
	}
	flag.Parse()

	var tmpIPList []string
	for _, s := range proxyIPAllowList {
		ip := net.ParseIP(s)
		if ip == nil {
			fmt.Printf("[Warning] Invalid IP %s specified for proxy-ip-allow\n", s)
			continue
		} else {
			proxyIPAllowListIP = append(proxyIPAllowListIP, ip)
			tmpIPList = append(tmpIPList, ip.String())
		}
	}

	fmt.Printf("Starting server at port %d\n", port)
	fmt.Printf("Expiring entries after %s\n", keepFor)
	fmt.Printf("Allowed Proxy IP Addresses: %s\n", strings.Join(tmpIPList, ", "))
	fmt.Printf("Remote Host Headers: %s\n", strings.Join(remoteHostHeaders, ", "))
	inventory := Inventory{}
	if inventory.Hosts == nil {
		inventory.Hosts = make(map[string]time.Time)
	}

	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/inventory", inventoryHandler(inventory, proxyIPAllowListIP, remoteHostHeaders, keepFor))

	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil); err != nil {
		log.Fatal(err)
	}
}
