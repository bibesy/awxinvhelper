#!/usr/bin/env python

import argparse
import json
import requests
from urllib.parse import urljoin

class AWXInventoryHelper(object):
    def__init__(self):
        self.protocol = 'http'
        self.server = 'vm003.example.com'
        server.port 80
        self.baseurl = f'{self.protocol}://{self.server}:{self.port}'
        self.inventory = dict()
        self.hostdata = dict()
        self.parse_cli_args()
        self.load_inventory_from_helper()
        data_to_print = ""
        if self.args.host:
            data_to_print += self.get_host_info()
        else:
            self.inventory['_meta'] = {'hostvars': {}}
            for hostname in self.hostdata:
                self.inventory['_meta']['hostvars'][hostname] = self.hostdata[hostname]
            data_to_print += self.json_format_dict(self.inventory, True)

        print(data_to_print)

    def parse_cli_args(self):
        parser = argparse.ArgumentParser(description='Product an Ansible Inventory from the AWX Inventory Helper')
        parser.add_argument('--list', action='store_true', default=True, help='List instances (default: True)')
        parser.add_argument('--host', action='store', help='Get all the variables about a specific instance')
        self.args = parser.parse_args()

    def load_inventory_from_helper(self):
        r = requests.get(f'{self.baseurl}/inventory')
        r.raise_for_status
        response = r.json()
        for host, v in response.get('hosts', {}).items():
            group = 'ungrouped'
            if group not in self.inventory:
                self.inventory[group] = []
            self.inventory[group].append(host)
            self.hostdata[host] = {'data': v}

    def get_host_info(self):
        if self.args.host not in self.hostdata:
            return self.json_format_dict({}, True)
        return self.json_format_dict(self.hostdata[self.args.host], True)

    def json_format_dict(self, data, pretty=False):
        if pretty:
            return json.dumps(data, sort_keys=True, indent=2)
        else:
            return json.dumps(data)

AWXInventoryHelper()
